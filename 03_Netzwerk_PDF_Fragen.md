## Wie lautet die Netzwerkadresse vom Standort Samedan?
192.168.3.0/24
## Auf welcher IP-Adresse befindet sich der AccessPoint in Bellinzona?
192.168.4.253
## In welchem VLAN befinden sich die Arbeitsplatz-PCs?
Auf dem VLAN 2
## Über welche IP-Adresse erreicht man den Manageable-Switch am Standort Chur?
192.168.2.253
## Welche IP-Adressen LAN-seitig und tunnelseitig ist der VPN-Gateway am Standort Bellinzona konfiguriert?
LAN-seitig: 192.168.4.1 , Tunnelseitig: 172.200.4.2
## Wie lautet der am Standort Samedan an den Arbeitsplatz-PCs eingetragene Standardgateway?
192.168.3.1
## Ein Aussendienstmitarbeiter benötigt Zugriff auf das Firmennetzwerk. Wie muss er seine VPN-SW IP-mässig konfigurieren?
Er braucht drei VPN's
172.200.3.1
172.200.4.1
172.200.5.1
## Wer hat im Büro CAD Wasserbau die VoIP-Telefone installiert?
abisang hat sie Installiert
## Wann wurde im Bistro der AccesPoint installiert?
Am 23.7.2014
## Der IT-Mitarbeiter Rene Sauter (rsauter) verlässt die Firma. Welche Arbeiten bzw. Verantwortlichkeiten sind davon betroffen? Wenn würden sie als zukünftigen Ansprechpartner bei Problemen vorschlagen?
E1/11, E2/1, E3/2, E8/1 sind die von rsauter. Der Rkunder soll sie übernehmen.
## Wieviele RJ45-Steckdosen stehen ihnen im Bauleiterbüro zur Verfügung und wieviele davon sind zurzeit noch nicht belegt?
32 zur Verfügung und 6 unbesetzt
## Im Büro CAD Tiefbau muss ein weiteres VoIP-Telefon zur Verfügung stehen. Wie soll diese Verbindung gepatched werden? Verlangt wird die Patchpanelbelegung und der Switch-Port.
Switch-Port : E2/2 und am besten zwischen den zwei Arbeitstischen
## Im Bistro soll eine Projektpräsentation stattfinden. Dafür muss ein Ethernetkabelverbindung bereitgestellt werden.
E9/1
## Im Büro CAD Wasserbau soll temporär ein weiterer Arbeitsplatz eingereichtet werden. Wie werden sie diese Aufgabe lösen?
Indem ich noch mehr Anschlüsse hinzufügen
## Wieviele Switchs stehen ihnen am Standort Chur zur Verfügung?
Zwei
## Frau Sommer arbeitet an der CAD-Workstation und meldet ihnen, dass sie keine Verbindung ins Internet mehr hat. Was werden sie überprüfen?
Den Gateway, den Ping ins Internet/Andere Mitarbeiter
## Wie lautet die SSID des Churer-AccessPoint?
NWA 1123-AC
hat Kontextmenü