## Aufgabe 1

Um eine Verbindung vom PC zum Switch herzustellen, navigiert man durch das IOS. Anschließend startet man eine Terminal-Sitzung auf dem PC. Dabei arbeitet man mit dem IOS und verwendet verschiedene Befehle. Es wird auch gezeigt, wie man in den privilegierten EXEC-Modus wechselt und die Uhrzeit einstellt